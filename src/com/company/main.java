package com.company;

import com.company.creational.abstractfactory.AbstractFactory;
import com.company.creational.abstractfactory.Card;
import com.company.creational.abstractfactory.FactoryProvider;
import com.company.creational.abstractfactory.PaymentMethod;
import com.company.creational.factorymethod.Payment;
import com.company.creational.factorymethod.PaymentFactory;
import com.company.creational.factorymethod.TypePayment;
import com.company.creational.prototype.PrototypeCard;
import com.company.creational.prototype.PrototypeFactory;
import com.company.creational.prototype.PrototypeFactory.CardType;

public class main {

	public static void main(String[] args) {
		testFactoryMethod();
		testAbstractFactory();
		testBuilder();
		testPrototype();
		testSingleton();
	}
	
	private static void testSingleton() {
		com.company.creational.singleton.Card card = com.company.creational.singleton.Card.getInstance();
		card.setCardNumber("1234-1234-1234-1234");
		System.out.println(card.toString());
	}

	private static void testPrototype() {
		PrototypeFactory.loadCard();
		try {
			PrototypeCard visa = PrototypeFactory.getInstance(CardType.VISA);
			visa.getCard();
			
			PrototypeCard amex = PrototypeFactory.getInstance(CardType.AMEX);
			amex.getCard();
		} catch (CloneNotSupportedException e) {
			e.getStackTrace();
		}
	}
	
	private static void testBuilder() {
		com.company.creational.builder.Card card = new com.company.creational.builder.Card.CardBuilder("VISA",
				"1111 2222 3333 4444")
				.name("Roo")
				.expires(2030)
				.credit(true)
				.build();
	
		System.out.println(card);
		
		com.company.creational.builder.Card card2 = new com.company.creational.builder.Card.CardBuilder("MASTERCARD",
				"5555 6666 7777 8888")
				.name("Roo")
				.credit(false)
				.build();
		
		System.out.println(card2);
	}

	private static void testAbstractFactory() {
		AbstractFactory<?> abstractFactoryCard = FactoryProvider.getFactory("CARD");

		Card card = (Card) abstractFactoryCard.create("VISA");

		AbstractFactory<?> abstractFactoryPaymentMethod = FactoryProvider.getFactory("PAYMENT_METHOD");

		PaymentMethod paymentMethod = (PaymentMethod) abstractFactoryPaymentMethod.create("CREDIT");

		System.out.println(
				"Tarjeta de tipo: " + card.getCardType() + " con el método de pago: " + paymentMethod.doPayment());

	}

	private static void testFactoryMethod() {
		Payment payment = PaymentFactory.buildPayment(TypePayment.GOOGLE_PAYMENT);
		payment.doPayment();
	}

}
