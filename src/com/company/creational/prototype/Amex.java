/**
 * 
 */
package com.company.creational.prototype;

/**
 * @author boris
 *
 */
public class Amex implements PrototypeCard {

	private String name;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public void getCard() {
		System.out.println("This is a Amex Card");
	}

	@Override
	public PrototypeCard clone() throws CloneNotSupportedException {
		System.out.println("Cloning Amex Card");
		return (Amex) super.clone();
	}

}
