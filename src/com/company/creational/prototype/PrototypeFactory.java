/**
 * 
 */
package com.company.creational.prototype;

import java.util.HashMap;
import java.util.Map;

/**
 * @author boris
 *
 */
public class PrototypeFactory {
	
	public static class CardType {
		public static final String VISA = "VISA";
		public static final String AMEX = "AMEX";
	}
	
	private static Map<String, PrototypeCard> prototypes = new HashMap<String, PrototypeCard>();
	
	public static PrototypeCard getInstance(final String type) throws CloneNotSupportedException {
		return prototypes.get(type).clone();
	}
	
	public static void loadCard() {
		Visa visa = new Visa();
		visa.setName("This is a Visa Card with number 0000");
		prototypes.put(CardType.VISA, visa);
		
		Amex amex = new Amex();
		amex.setName("This is a Amex card with number 1111");
		prototypes.put(CardType.AMEX, amex);
	}
	
}
