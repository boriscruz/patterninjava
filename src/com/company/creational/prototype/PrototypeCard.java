/**
 * 
 */
package com.company.creational.prototype;

/**
 * @author boris
 *
 */
public interface PrototypeCard extends Cloneable {

	void getCard();
	
	PrototypeCard clone() throws CloneNotSupportedException;
	
}
