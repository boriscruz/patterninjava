/**
 * 
 */
package com.company.creational.prototype;

/**
 * @author boris
 *
 */
public class Visa implements PrototypeCard {

	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public void getCard() {
		System.out.println("This is a Visa Card");
	}

	@Override
	public PrototypeCard clone() throws CloneNotSupportedException {
		System.out.println("Cloning Visa Card");
		return (Visa) super.clone();
	}

}
