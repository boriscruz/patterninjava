/**
 * 
 */
package com.company.creational.singleton;

/**
 * @author boris
 *
 */
public class Card {

	private static Card INSTANCE;

	private String cardNumber;

	private Card() {
	}

	public synchronized static Card getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new Card();
		}
		return INSTANCE;
	}

	public static Card getINSTANCE() {
		return INSTANCE;
	}

	public static void setINSTANCE(Card iNSTANCE) {
		INSTANCE = iNSTANCE;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	@Override
	public String toString() {
		return "Card [cardNumber=" + cardNumber + "]";
	}

}
