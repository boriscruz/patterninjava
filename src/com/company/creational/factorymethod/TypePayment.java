package com.company.creational.factorymethod;

public enum TypePayment {
	CARD, GOOGLE_PAYMENT
}
