package com.company.creational.factorymethod;

public class CardPayment implements Payment {

	@Override
	public void doPayment() {
		System.out.println("Paying with credit card");
	}

}
